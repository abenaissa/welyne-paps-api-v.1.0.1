def verify_data(data1):
    try:
     if(len(data1["vehicles"])>0):
         vehicle_types=["scooter","mini van","van","truck"]
         keys = data1["vehicles"].keys()
      
         cond=False
         result=()
         for key in list(set(keys)):
        
           if(key.lower() not in vehicle_types):
              
              cond=True
              return 400,"you provided the wrong type of vehicles make sure that the vehicles follow the following types:\n van \n truck \n scooter \n mini van"
         if(cond==False):
           result=(200,True)
         else:
            result=( 400,"You did not provide the list of vehicles or wrong type of vehicle")
            return result
    except KeyError as k:
       return 400,"provide the list of vehicles"
    try:
     if( result == (200,True) and len(data1["missions"])>0):
        for data in data1["missions"]: 
            if(data["tasks"] is  None or len(data["tasks"])==0):
                return 400,"You did not provide the list of tasks"
            else:
            # print(len(data["tasks"]))        
             for task in data['tasks']:
               
                try :
                 mission_id=data['uid']
                 action=task['action']
                 
                 if(mission_id is None or len(mission_id)==0):
                    return 400,"You did not provide the Id of the mission"
                 """""
                 mission_type = data['type']
                 print(data['type'],mission_id)
                 if(mission_type is None or len(mission_type)==0):
                    return 400,"You did not provide the type of the mission"
                 """
                except KeyError as k:
                   print(k)
                   return 400,"provide the type and the ID of the mission or the action"
                
                try:
                  status=task["status"]
                  print(status)
                  if(status is None or len(status)==0):
                    return 400,"You did not provide the status of the task"
                except KeyError as kerr:
                    return 400,"You must provide the status of the task and it has to match the following values == NOT_STARTED,DONE,ASSIGNED"
                
               
                   
            
                if(len(task["stops"])==0 or task['stops']==None):
                    return 400,"Please provide the list of stops"
                else:
                 number_of_stops = len(task['stops'])
                
                 for i in range(number_of_stops):
                    try:
                     action=task['stops'][i]['action']
                  #   print("action===",action)
                     if(action is None or len(action)==0):
                            return 400,"provide the action type whether is it a delivery or pickup"
                     if(action.lower()  not in ["delivery","pickup","delivery&pickup (entropot)"]):
                            return 400,"verify the action type again.."
                     if(status not in ["NOT_STARTED","DONE","ASSIGNED"]):
                        return "task status has to match the following values == NOT_STARTED,DONE,ASSIGNED" 

                    except KeyError as k:
                        return 400,"provide the action type whether is it a delivery or pickup"
                    try:
                     action_address=task['stops'][i]['address']['address']
                     if(action_address is None or len(action_address)==0):
                         return 400,"provide the address"
                    except KeyError as K:
                        return 400,"provide the address"
                    try:
                       length=len(task["stops"][i]["items"])
                    except KeyError as k :
                       return 400,"provide the list of Items"
                       
                    if(len(task["stops"][i]["items"])==0 or (task["stops"][i]["items"]) is None):
                         return 400,"provide the list of Items"
                    else:
                      for item in task['stops'][i]['items']:
                      
                        try :
                           parcel_id=item['product']['parcelId']
                        except KeyError as k:
                           return 400,"provide the parcel ID" 
                        try :
                           weight=item['product']['weight']
                        except KeyError as k:
                           return 400,"provide the weight of the product" 
                        if(len(item['product']['parcelId'])==0 or item['product']['parcelId'] is None):
                            return 400,"provide the parcel ID" 
                        
                        if(item['product']['weight']==0 or item['product']['weight'] is None):
                            return 400,"weight of the product must be a valid value"
                        else:
                         try :
                            
                            if parcelId!=item['product']['parcelId']:
                                
                                parcelId=item['product']['parcelId']
                               
                                parcelVolume=item['product']['weight']
                         except Exception as e:
                                parcelId=item['product']['parcelId']
                                
                                parcelVolume=item['product']['weight']
        return result
     else:
        return 400,"provide the list of missions"
    except KeyError as k : 
       return 400,"where are the missions ?"
    


                                

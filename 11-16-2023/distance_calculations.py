import random
import requests
from ortools.constraint_solver import pywrapcp, routing_enums_pb2

def calculate_total_distance(distance_matrix, route):
    total_distance = 0.0
    for i in range(1, len(route)):
        from_location = route[i - 1]
        to_location = route[i]
        distance = distance_matrix[from_location][to_location]
        total_distance += distance
    return total_distance

import threading
def get_matrix(data,distances,i,j):
    try:
     if 'rows' in data and len(data['rows']) > 0 and 'elements' in data['rows'][0] and 'distance' in data['rows'][0]['elements'][0]:
                distance = data['rows'][0]['elements'][0]['distance']['value'] / 1000
                distances[i][j] = distance
                distances[j][i] = distance  # Distance matrix is symmetric
   
     print("p---------->",distances)
     return distances
    except Exception as e : 
        print("error:",e)

def calculate_distance(api_key, origin, destination, distances, i, j,error_flag):

    try:
     url = 'https://maps.googleapis.com/maps/api/distancematrix/json'
     params = {
        'key': api_key,
        'origins': origin,
        'destinations': destination,
    
     }
     response = requests.get(url, params=params)
     data = response.json()
  

     if error_flag[0] == 0:
      thread = threading.Thread(
                target=get_matrix,
                args=(data,  distances, i, j)
            )
      thread.start()
      thread.join()
    except requests.exceptions.ConnectionError as e:
     print("erorr here=",e)
     error_flag[0] = 1
     return e
  
from flask import jsonify

def calculate_distances(locations,api_key):

    num_locations = len(locations)
    distances = [[0] * num_locations for _ in range(num_locations)]
    error_flag = [0]
    threads = []
    try : 
     for i in range(num_locations):
        for j in range(i + 1, num_locations):
            try:
             thread = threading.Thread(
                target=calculate_distance,
                args=(api_key, locations[i], locations[j], distances, i, j,error_flag)
            )   
             threads.append(thread)
             thread.start()
             if error_flag[0] == 1:
                print("Api is down due to a network error")
                
                
                break;
            except Exception as e:
                return str(e)
        if error_flag[0] == 1:
          #  break
           return jsonify({"error":"network error ....."}),400
            
     if(error_flag[0]==1):
         return 'error'
     else:   
      try:           
       for thread in threads:
        thread.join()
       return distances
      except Exception as k:
         return k
    except Exception as e1:
        print("error=====================================",e1)
        return str(e1)
 
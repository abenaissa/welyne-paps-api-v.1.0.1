


warehouse="152 Rue DD 49, Dakar, Senegal"




def find_pickup_ops(runsheet,ls):
  pickups=[]
  for x in runsheet:
    if(x[0]=="DELIVERY"):
      for e in ls:
        if(x[2]==e[2] and e[0]=="PICKUP" and e not in runsheet):
          pickups.append(e)
  return pickups        
  

import time

from distance_calculations import calculate_total_distance
from threading_coordinates import get_coordinates_many 
start_time=time.time()
warehouse="152 Rue DD 49, Dakar, Senegal"
warehouse_address="152 Rue DD 49, Dakar, Senegal"
def get_calculations_params(deliveries):
  nb=0
  for u in deliveries:
    if("dakar"  in u[1].lower()):
      nb=nb+1
  return len(deliveries)-nb

def choose_start_points(deliveries,ls,locations_list,mtx,nb):
  distances=[]
  for e in deliveries:
    e[1].lower()
    if("dakar" not in e[1].lower() and  'thies' not in e[1].split(",")[0].lower() and "pout" not in e[1].lower()):
     print(e[1],warehouse_address,":")
     print(mtx[locations_list.index(e)-1][locations_list.index(e)])
     distance=mtx[locations_list.index(e)-1][locations_list.index(e)]
     
     
     if(distance<130):
       distances.append({"destination":e,'distance':distance})
  
  l=list(sorted(distances, key=lambda x: x["distance"]))
  l1=[]
  l2=[]
  for x in l:
   
     print(x)
     if(x["distance"]<120):
        l1.append(x)
     else:
        l2.append(x)

  end=int(nb/2)
  print(l1)
  print(l2)
  lst1=l1[0:len(l1)-1]+l2
  
  lst=lst1[0:end-1]
  print(lst)
  lst1=list(sorted(lst, key=lambda x: x["distance"]))
  runsheets_init=[]
  for v in lst1:
    runsheet_init=[]
    for e in ls :
      if(v["destination"][2]==e[2]):
        runsheet_init.append(e)
        runsheet_init.append(v['destination'])
        break
    print(runsheet_init)
    runsheets_init.append(runsheet_init)
  return runsheets_init 

def choose_start_points2(deliveries,ls,locations_list,mtx):
  distances=[]
  print("-------------------------------------------------------------")
  print("************")
  print("-------------------------------------------------------------")
  for e in deliveries:
    if("dakar" not in e[1].lower()):
     print(mtx[locations_list.index(e)-1][locations_list.index(e)])
     distance=mtx[locations_list.index(e)-1][locations_list.index(e)]
    # print(float(get_distance(warehouse,e[1])))
     distances.append({"destination":e,'distance':distance})
  l=list(sorted(distances, key=lambda x: x["distance"]))
  print(l[0:2])
  lst=l[0:2]
  lst1=list(sorted(lst, key=lambda x: x["distance"]))
  runsheets_init=[]
  for v in lst1:
    runsheet_init=[]
    for e in ls :
      if(v["destination"][2]==e[2]):
        runsheet_init.append(e)
        runsheet_init.append(v['destination'])
        break
    print("runsheet_init===========",runsheet_init)
    runsheets_init.append(runsheet_init)
  return runsheets_init
         
def find_pickup_ops(runsheet,ls):
  pickups=[]
  for x in runsheet:
    if(x[0]=="DELIVERY"):
      for e in ls:
        if(x[2]==e[2] and e[0]=="PICKUP" and e not in runsheet):
          pickups.append(e)
  return pickups        
  
def find_shortcut(route,nb,deliveries1,locations_list,mtx):
 
  start=route[len(route)-1]
  try:
   deliveries1.remove(start)
  except Exception as e : 
    print("start****=",start)
  lats=[]
  lats0,lat1,lg=get_coordinates_many(nb,start,lats,deliveries1)
  lats1=list(sorted(lats0, key=lambda x: x["latitude"])) 
  deliveries2=[]
  for l_t in lats1[0:nb]:
     print(l_t["delivery"][1],l_t["latitude"])
     if(l_t["latitude"]<lat1):
        deliveries2.append(l_t["delivery"])  
  c=2
  total_distance=mtx[locations_list.index(route[0])][locations_list.index(start)]
  print(total_distance)
  while(total_distance<=210):
   c=c+1
   start=route[-1]
   print("start=",start)
   distances=[]
   for x in deliveries2:
     if("dakar" not in x[1].lower()):
      if(start[1]!=x[1] and x not in route):
         print(x[1])
         distance=mtx[locations_list.index(start)][locations_list.index(x)]
         print(mtx[locations_list.index(start)][locations_list.index(x)])
         distances.append({"destination":x,'distance':distance})
   try:
    min_value = min(distances, key=lambda x: x['distance'])["destination"]
    total_distance=total_distance+mtx[locations_list.index(start)][locations_list.index(min_value)]
    if(total_distance>=210):
       break
    print('total_distance=',total_distance)
    print("->",min_value)
    route.append(min_value)
    deliveries1.remove(min_value)
    start=min_value
   except Exception as e :
     break
  return route
def find_shortcut_truck(route,nb,deliveries1,locations_list,mtx):
  start=route[len(route)-1]
  try:
   deliveries1.remove(start)
  except Exception as e : 
    print("start****=",start)

  c=2
  total_distance=mtx[locations_list.index(route[0])][locations_list.index(start)]
  print(total_distance)
  while(total_distance<=500):
   c=c+1
   start=route[-1]
   print("start=",start)
   distances=[]
   print("deliveries1===",deliveries1)
   for x in deliveries1:
     if("dakar" not in x[1].lower()):
      if(start[1]!=x[1] and x not in route):
         print(x[1])
         distance=mtx[locations_list.index(start)][locations_list.index(x)]
         print(mtx[locations_list.index(start)][locations_list.index(x)])
         distances.append({"destination":x,'distance':distance})
   try:
    min_value = min(distances, key=lambda x: x['distance'])["destination"]
    total_distance=total_distance+mtx[locations_list.index(start)][locations_list.index(min_value)]
    if(total_distance>=500):
       break
    print('total_distance=',total_distance)
    print("->",min_value)
    route.append(min_value)
    deliveries1.remove(min_value)
    start=min_value
   except Exception as e :
     break
  return route


  


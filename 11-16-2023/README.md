English version : 

Description :

 the purpose of this api is to generate Run-sheets for each type of vehicle these  Run-sheets includes Shortcuts by shortcut we mean  the shortest route possible .the choice of vehicle for each route depends on the cargo payload of the vehicle and the weight of the items carried 

 Install dependencies and packages from requirements.txt

 Run this  project on your local machine :   http://127.0.0.1:5000/api/v1/runsheet-proposal 


French version

Description

  Le but de cette API est de générer des feuilles de route pour chaque type de véhicule. Ces feuilles de route incluent des raccourcis. Par raccourci, nous entendons le chemin le plus court possible. Le choix du véhicule pour chaque itinéraire dépend de la charge utile du véhicule et du poids des articles transportés


 Installer les dépendances et les packages à partir du fichier requirements.txt.

 Exécutez ce projet sur votre machine locale : http://127.0.0.1:5000/api/v1/runsheet-proposal




import random
import requests
from ortools.constraint_solver import pywrapcp, routing_enums_pb2

def calculate_total_distance(distance_matrix, route):
    total_distance = 0.0
    for i in range(1, len(route)):
        from_location = route[i - 1]
        to_location = route[i]
        distance = distance_matrix[from_location][to_location]
        total_distance += distance
    return total_distance

import threading
def get_matrix(data,distances,i,j):
    try:
     if 'rows' in data and len(data['rows']) > 0 and 'elements' in data['rows'][0] and 'distance' in data['rows'][0]['elements'][0]:
                distance = data['rows'][0]['elements'][0]['distance']['value'] / 1000
                distances[i][j] = distance
                distances[j][i] = distance  # Distance matrix is symmetric
   
     
     return distances
    except Exception as e : 
        print("error:",e)

def calculate_distance(api_key, origin, destination, distances, i, j,error_flag):

    try:
     url = "https://maps.googleapis.com/maps/api/distancematrix/json"
    
     params = {
        "origins": origin,
        "destinations": destination,
        "key": api_key
     }
     response = requests.get(url, params=params)
     data = response.json()
     print(origin,"-->",destination,":",data)  

     if error_flag[0] == 0:
      thread = threading.Thread(
                target=get_matrix,
                args=(data,  distances, i, j)
            )
      thread.start()
      thread.join()
    except requests.exceptions.ConnectionError as e:
     print("=========","Google maps api is unreachable for the moment please check your internet speed","==========")
     error_flag[0] = 1
     return e
  
from flask import jsonify

def calculate_distances(locations,api_key):
    
    num_locations = len(locations)
    distances = [[0] * num_locations for _ in range(num_locations)]
    error_flag = [0]
    threads = []
    try : 
     n_b=0
     for i in range(num_locations):
        for j in range(i + 1, num_locations):
            n_b=n_b+1
            print("n________________________________b=",n_b)
            try:
             thread = threading.Thread(
                target=calculate_distance,
                args=(api_key, locations[i], locations[j], distances, i, j,error_flag)
            )   
             import time 
             if(num_locations>8):
               if(n_b>=6):
                if(num_locations<=13):
                 time.sleep(0.10)
                 n_b=0
                else:
                 time.sleep(0.15)
                 n_b=0
                   
             threads.append(thread)
             thread.start()
             if error_flag[0] == 1:
                print("Api is down due to a network error")
                
                
                break;
            except Exception as e:
                return str(e)
        if error_flag[0] == 1:
          #  break
           return jsonify({"error":"network error ....."}),400
            
     if(error_flag[0]==1):
         return 'error'
     else:   
      try:           
       for thread in threads:
        thread.join()
       return distances
      except Exception as k:
         return k
    except Exception as e1:
        print("error=====================================",e1)
        return str(e1)
      

def get_route_distances(locations, api_key):
    url = 'https://maps.googleapis.com/maps/api/distancematrix/json'
    # print('we ')
    num_locations = len(locations)
    distances = [[0] * num_locations for _ in range(num_locations)]  # Initialize the distances matrix
   


    for i in range(num_locations):
        for j in range(i + 1, num_locations):
            params = {
                'key': api_key,
                'origins': locations[i],
                'destinations': locations[j],
                
            }
          

            try:
                response = requests.get(url, params=params)
                response.raise_for_status()
                data = response.json()
                if 'rows' in data and len(data['rows']) > 0 and 'elements' in data['rows'][0] and 'distance' in data['rows'][0]['elements'][0]:
                    distance = data['rows'][0]['elements'][0]['distance']['value'] / 1000

                    distances[i][j] = distance
                    distances[j][i] = distance  # Distance matrix is symmetric
        

            except (requests.exceptions.RequestException, ValueError) as e:
                print(f"Error processing locations {i} and {j}: {e}")


    return  distances #,durations



def seconds_to_hms(seconds):
    hours = seconds // 3600
    minutes = (seconds % 3600) // 60
    seconds = seconds % 60
    return f"{hours}h {minutes}m {seconds}s"

def choose_starting_point(distance_matrix):
    num_locations = len(distance_matrix)
    
    # Create a list of all location indices
    location_indices = list(range(num_locations))

    # Calculate the sum of distances to the top three closest locations for each location
    sum_distances = [sum(sorted(distance_matrix[i])) for i in location_indices]
    
    # Find the location with the minimum sum of distances
    best_starting_point = location_indices[sum_distances.index(min(sum_distances))]
    
    
    return best_starting_point

def split_route(route, distance_matrix, max_distance=1):
    sub_routes = []
    sub_route = [0]
    distance_sum = 0

    for i in range(1, len(route)):
        distance_sum += distance_matrix[route[i - 1]][route[i]]
        if distance_sum > max_distance:
            if len(sub_route) > 1:  # Avoid sub-routes with only the starting location
                sub_routes.append(sub_route)
            sub_route = [0]
            distance_sum = 0

        sub_route.append(route[i])

    sub_routes.append(sub_route)
    return sub_routes

def generate_google_maps_link(locations):
    base_url = "https://www.google.com/maps/dir/"
    
    # Convert each location to a format suitable for the URL
    formatted_locations = [location.replace(' ', '+') for location in locations]
    
    # Combine the formatted locations to create the complete URL
    full_url = base_url + '/'.join(formatted_locations)
    
    return full_url

def combine_consecutive_occurrences(lst):
    combined_occurrences = []
    current_element = lst[0]

    for i in range(1, len(lst)):
        if lst[i] == current_element:
            continue
        else:
            combined_occurrences.append(current_element)
            current_element = lst[i]

    combined_occurrences.append(current_element)
    return combined_occurrences



from flask import Flask, request, jsonify
from final_algorithm import optimize_delivery_multiple_missions
from flask_cors import CORS
import numpy as np
from requests.exceptions import SSLError
from find_optimize_route import check_if_delivery_is_done
app = Flask(__name__)
from flask_swagger_ui import get_swaggerui_blueprint
from input_control import verify_data
SWAGGER_URL="/swagger"
API_URL="/static/swagger.json"

swagger_ui_blueprint = get_swaggerui_blueprint(
    SWAGGER_URL,
    API_URL,
    config={
        'app_name': 'Delivery Management'
    }
)

CORS(app, resources={r"/api/v1/runsheet-proposal": {"origins": "*"}})  
CORS(app) 
    
@app.route('/api/v1/runsheet-proposal', methods=['POST'])
def make_runsheet():
    try:
        # Get the JSON data from the frontend request
        if(verify_data(request.get_json())!=(200,True)):
            c,e=verify_data(request.get_json())
            print(e)
            return ({'error': e}), 400
        
        data_missions = request.get_json()["missions"]
       # print(data_missions)
        check_if_delivery_is_done(data_missions)
        vehicles=request.get_json()["vehicles"]

      
        mission_tasks = []
        mission_id=[]
        locations_list = []
        entropot_PICKUP_data=[]
        ENTROPOT=False
        entropot_notseen_parcels=[]
        dictionarry_map_PID_RANK={}
        dictionarry_map_PID_action={}
        rank=-1
        runsheet_response={}
        for data in data_missions:           
            for task in data['tasks']:
                mission_id=data['uid']
                
                try:
                 mission_type = data['type']
                 productType=data['productType']
                except KeyError as k :
                    productType=[]
                    mission_type="msg"
              
                number_of_stops = len(task['stops'])
                for i in range(number_of_stops):
                  
                    action=task['stops'][i]['action']
                    ENTROPOT=False
    
                    action_address=task['stops'][i]['address']['address']
                    for item in task['stops'][i]['items']:
                        rank+=1
                        try :
                            if parcelId!=item['product']['parcelId']:
                                
                                parcelId=item['product']['parcelId']
                              
                                parcelVolume=item['product']['weight']
                        except Exception:
                                parcelId=item['product']['parcelId']
                               
                                parcelVolume=item['product']['weight']
                                


                        


                       
                        if ENTROPOT==True:
                            

                            
                            if parcelId not in entropot_notseen_parcels:
                              
                                entropot_notseen_parcels.append(parcelId)
                                
                                
                               
                               
                                
                                dictionarry_map_PID_RANK.update({parcelId : rank})

                                task_dict = {
                                    'mission_id': mission_id,
                                    'mission_type': mission_type,
                                    
                                    'productType': productType,
                                    '_id': task['_id'],
                                    'action': task['action'],
                                    'status': task['status'],
                                    'stops_action': task['stops'][i]['action'],
                                  
                                    'stops_address': action_address,
                                    'item_product_parcelUid': item['product']['parcelUid'],
                                    'item_product_parcelVolume': parcelVolume,
                                }


                                dictionarry_map_PID_action.update({parcelId : task_dict})


                               
                            else:
                                entropot_notseen_parcels.remove(parcelId)
                                RANK=dictionarry_map_PID_RANK[parcelId]
                                action='DELIVERY&PICKUP (ENTROPOT)'
                                locations_list.insert(RANK,(action, action_address, parcelId, parcelVolume))


                              
                                action='DELIVERY'
                              
                                mission_tasks.insert(RANK,{
                        'uid': mission_id,
                        'type': mission_type,
                        # 'vehicleType': vehicleType,
                        'productType' : productType,
                        'tasks' : [{

                            '_id': task['_id'],
                            'action': task['action'],
                            'status': task['status'],
                            'stops': [{
                                'action': action,
                                'address':{
                                   # 'place_id':task['stops'][i]['address']['place_id'],
                                    #'region' : task['stops'][i]['address']['region'],
                                    'address':action_address, 
                                        },
                            'items': [
                                {
                                'product':{
                                    'parcelUid':item['product']['parcelUid'],
                                    'parcelVolume':parcelVolume#,
                                }
                                    }
                                    ]
                            }]

                    }]
                    }
                    )
                                #print('post insert')
                                action='PICKUP'
                                entropot_PICKUP_data.insert(RANK,{

                            'uid': mission_id,
                            'type': mission_type,
                            
                            'productType' : productType,
                            'tasks' : [{

                                '_id': task['_id'],
                                'action': task['action'],
                                'status': task['status'],
                                'stops': [{
                                    'action': action,
                                    'address':{
                                      #  'place_id':task['stops'][i]['address']['place_id'],
                                       # 'region' : task['stops'][i]['address']['region'],
                                        'address':action_address, 
                                            },
                                'items': [
                                    {
                                    'product':{
                                        'parcelUid':item['product']['parcelUid'],
                                        'parcelVolume':parcelVolume#,
                                    }
                                        }
                                        ]
                                }]

                        }]
                        })
                                break
                        else:
                            
                            locations_list.append((action, action_address, parcelId, parcelVolume))

                            mission_tasks.append({
                        'uid': mission_id,
                        'type': mission_type,
                        # 'vehicleType': vehicleType,
                        'productType' : productType,
                        'tasks' : [{

                            '_id': task['_id'],
                            'action': task['action'],
                            'status': task['status'],
                            'stops': [{
                                'action': action,
                                'address':{
                                  #  'place_id':task['stops'][i]['address']['place_id'],
                                   # 'region' : task['stops'][i]['address']['region'],
                                    'address':action_address, 
                                        },
                            'items': [
                                {
                                'product':{
                                    'parcelUid':item['product']['parcelUid'],
                                    'parcelVolume':parcelVolume#,
                                }
                                    }
                                    ]
                            }]

                    }]
                    }
                    

                            )
                            entropot_PICKUP_data.append({'marhbe': 0})
                       

       
        i=-1
        for pid in entropot_notseen_parcels:
           
            task_dict = dictionarry_map_PID_action.get(pid, {})

            RANK=dictionarry_map_PID_RANK[pid]
            i+=1
           
            
            action_address=locations_list.insert(RANK,(task_dict.get('stops_action'), task_dict.get('stops_address'), pid, task_dict.get('item_product_parcelVolume')))
           


            
            mission_tasks.insert(RANK, {
                'uid': task_dict.get('mission_id'),
                'type': task_dict.get('mission_type'),
                
                'productType': task_dict.get('productType'),
                'tasks': [{
                    '_id': task_dict.get('_id'),
                    'action': task_dict.get('action'),
                    'status': task_dict.get('status'),
                    'stops': [{
                        'action': task_dict.get('stops_action'),
                        'address': {
                          #  'place_id': task_dict.get('stops_place_id'),
                           # 'region': task_dict.get('stops_region'),
                            'address': task_dict.get('stops_address'),
                        },
                        'items': [
                            {
                                'product': {
                                    'parcelUid': task_dict.get('item_product_parcelUid'),
                                    'parcelVolume': task_dict.get('item_product_parcelVolume'),
                                }
                            }
                        ]
                    }]
                }]
            })
            entropot_PICKUP_data.insert(RANK,{'marhbe': 0})
            
        
        
        for element in mission_tasks:
            try:
                if element is not None:
                    address = element.get('tasks', [{}])[0].get('stops', [{}])[0].get('address', {}).get('address')
                    ac= element.get('tasks', [{}])[0].get('stops', [{}])[0].get('action')
                    if address is not None:
                  
                        pass
            except Exception :
               
                pass
                continue
      
        for element in entropot_PICKUP_data:
            try:
                if element !={'marhbe': 0}:
                    address = element.get('tasks', [{}])[0].get('stops', [{}])[0].get('address', {}).get('address')
                    ac= element.get('tasks', [{}])[0].get('stops', [{}])[0].get('action')
                    if address is not None:
                 
                        pass
                else:
                   
                    pass
            except Exception:
                
                pass
                continue

        def find_duplicates(lst):
            seen = set()
            duplicates = set()

            for item in lst:
                if item in seen:
                    duplicates.add(item)
                else:
                    seen.add(item)
            return list(duplicates)
        redundant_elements = find_duplicates(locations_list)

        try:
         
          
            vehicleType,mission_route, data_list, google_maps_link_list,full_link = optimize_delivery_multiple_missions(data_missions,locations_list,mission_tasks,entropot_PICKUP_data,vehicles)
            runsheet_response = {
            "code": 200,
            "data": [],
            'status': 'success',
            'message': 'Successful operation'
        }

            for vehicule, mroute, data, google_maps_link in zip(vehicleType, mission_route, data_list, google_maps_link_list):
                runsheet_response["data"].append({
                'description': mroute,
                'google_maps_link_list': google_maps_link,
                'vehiculeType': vehicule,
                f'Runsheet{len(runsheet_response["data"]) + 1}': data
            })
           
           
            print(full_link)
        
            return jsonify(runsheet_response), 200

       
        except Exception as e:
            error_value="api is down due to network issue"
            return jsonify({'error': error_value}), 400  
        
       

        

       
       

        

        
    except Exception as e:
        print(e)
        # Handle errors and return an OpenAPI-compliant error response
        error_response = {
            'status': 'error',
            'code': 400,
            'message': e
        }
        return jsonify(error_response)
    
    
    
                                            

if __name__ == '__main__':
   # app.register_blueprint(swagger_ui_blueprint, url_prefix=SWAGGER_URL)
    app.run(host='0.0.0.0', port=5000,debug=True)

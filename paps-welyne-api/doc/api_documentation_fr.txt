API Welyne - OpenAPI 1.0 Documentation

Renseignements généraux

Title: API Welyne - OpenAPI 1.0

Description : Exemple d’API pour Welyne basé sur la spécification OpenAPI 3.0.Version: 1.0.1

Endpoint:POST /runsheet-proposal

Résumé : Générer des runsheetsDescription : 
 le but de cette api est de générer des Run-sheets pour chaque type de véhicule, ces Run-sheets incluent des raccourcis par raccourci, nous entendons le plus court itinéraire possible. le choix du véhicule pour chaque itinéraire dépend de la charge utile du véhicule et du poids des articles transportés



Paramètres : X-Correlation-ID (header) : Identifiant unique pour référencer une transaction.Corps de la demande : Requis. Proposez des runsheets au format JSON, XML ou URL. Le corps de la requête contient des données sur les véhicules et les missions Demande Body 
  Parametersvehicles (objet, requis)

 Description : Contient des renseignements sur les véhicules disponibles pour la mission.scooter (objet)
 charge utile (nombre) : Capacité de charge utile maximale du scooter en kilogrammes.
 max_distance (nombre) : Distance maximale que le scooter peut parcourir en kilomètres.mini van (objet)
 charge utile (nombre) : Capacité de charge utile maximale du mini van en kilogrammes.
 max_distance (nombre) : Distance maximale que la mini-van peut parcourir en kilomètres.van (objet)
 payload (nombre) : Capacité de charge maximale du van en kilogrammes.
 max_distance (nombre) : Distance maximale que la van peut parcourir en kilomètres.camion (objet)
 charge utile (nombre) : Capacité de charge utile maximale du camion en kilogrammes.
 max_distance (nombre) : Distance maximale que le camion peut parcourir en kilomètres.missions (réseau, requis) 
 :Description : Contient des détails sur la mission à créer.uid (string) : Identifiant unique pour la mission.client (objet) :
 Détails sur le client qui demande la mission.client_company_name (string) :
 Nom de l’entreprise du client.client_phone_number (string) : 
 Numéro de téléphone du client.client_contact_email (string) : 
 adresse e-mail du client.client_address (objet) : 
 Adresse détaillée du client.adresse (string) : adresse municipale.pays (string) : Pays.ville (string) :
 Ville.région (string) : Région.status (string) 
 Statut de la mission (ex : "NOT_STARTED").type (string) 
 Type de mission.executionDate (string) :
 Date d’exécution de la mission (format : "AAAA-MM-JJ").slotStart (string)
    : Heure de début de la fente de mission (format : "HH:MM").slotEnd (string) 
   : Heure de fin de l’emplacement de la mission (format : "HH:MM").createdAt (string) 
   : Date et heure de création de la mission (format : "YYYY-MM-DDTHH:MM:SS.ZZZZ").vehicleType (string) :   
   Type de véhicule à utiliser pour la mission.productType (tableau) : Liste des types de produits.tâches (tableau) : 
   Liste des tâches associées à la mission._id (string) : Identificateur unique pour la tâche.type (string) :
   Type de tâche.action (string) : Action à effectuer pour la tâche.status (string) : 
   Statut de la tâche (par exemple, "NOT_STARTED").isReturn (booléen) : 
   Indique si la tâche est une tâche retournée.stops (tableau) : 
   Liste des stops pour la tâche.createdAt (string) : 
   Date et heure de création de l’arrêt (format : "YYYY-MM-DDTHH:MM:SS.ZZZZ").libelle (string) : 
   Étiquette pour l’arrêt.action (string) : Action à exécuter à l’arrêt.isDisabled (booléen) :
    Indique si l’arrêt est désactivé.adresse (objet) : Adresse détaillée de l’arrêt.place_id (string) : Place 
ID.region (string) : Region.
 adresse (string) : adresse municipale.items (tableau) : Liste des items associés à l’arrêt.produit (objet) 
 : Détails du produit.name (string) : Nom du produit.parcelUid (string) : 
 Parcel UID.parcelId (string) : Parcel 
ID.weight (nombre) : Poids du produit en kilogorams




Format de réponse 
{
  "code" : 200,
  "data" : [
    {
      "Runsheet1" : { ... },
      "Runsheet2" : { ... },
      ...
    }
  ]
  "message" : "Opération réussie",
  "status" : "success"
  }


{
  "code" : 400,
  "message" : "Bad Request : Invalid parameters provided",
  "status" : "error"
}

{
  "code" : 400,
  "message" : "Bad Request : api is down due to a network error ",
  "status" : "error"
}


{
  "code" : 401,
  "message" : "Unauthorized : Invalid API key",
  "status" : "error"
}


Responses:200 : Opération réussie, retourne les détails de la proposition de runsheet contenant un raccourci pour chaque itinéraire de véhicule.
 
400: Bad Request.
 
401 : Non autorisé. 

403 : Interdit. Page non trouvée.
 
Sécurité : Nécessite Google Maps api_key avec des autorisations pour calculer les distances entre les emplacements. 
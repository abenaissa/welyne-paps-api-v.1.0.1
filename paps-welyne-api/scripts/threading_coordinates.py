import concurrent.futures

# Example implementation of get_latitude function
def get_latitude(location):
    from scripts.get_coordinates import get_latitude
    return get_latitude(location)

# Example data
start = ["start", "some_value"]


def process_location(location):
    return get_latitude(location)


import time
def get_coordinates_many(nb,start,lats,deliveries1):
    start_time = time.time()
    
    with concurrent.futures.ThreadPoolExecutor() as executor:
        # Submit tasks to the executor and create a mapping of future to location
        future_to_delivery = {executor.submit(process_location, d[1]): d[1] for d in deliveries1 if "dakar" not in d[1].lower()}
        lat1,lg1=get_latitude(start[1])
        
        
        # Process the results as they are completed
        for future in concurrent.futures.as_completed(future_to_delivery):
           # print(future.result)
            location = future_to_delivery[future]
            d=()
            for x in deliveries1:
                if(x[1]==location):
                    d=x
                  #  print(d)
                    break

            try:
               if(d!=start): 
                lat, lg = future.result()
                
                k=(nb/2)-1
                if(len(deliveries1)<=int(k)):
                       lats.append({"delivery":d,"latitude":lat})
                else: 
                   if(abs(lat-lat1)<0.3):
                      lats.append({"delivery":d,"latitude":lat})
                      if(abs(lg1-lg)>=1 ) :
                         lats.remove({"delivery":d,"latitude":lat})

            except Exception as exc:
                print("")
    end_time = time.time()  # Record the end time
    execution_time = end_time - start_time  # Calculate the execution time
    print("")
    return lats,lat1,lg1



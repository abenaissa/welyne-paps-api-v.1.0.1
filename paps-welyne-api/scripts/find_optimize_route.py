import numpy as np

from scripts.distance_calculations import calculate_total_distance
from scripts.test_file import choose_start_points, choose_start_points2, find_pickup_ops, find_shortcut, get_calculations_params,find_shortcut_truck

warehouse_address="152 rue dd 49"
mini_van="mini van"
def find_closest_unvisited_location(distance_matrix, current_location,
               visited,UI1,locations_mapping1,
                 locked_entrepot1,locked_delivery1, locations_list):
    closest_location = None
   
    min_distance = float('inf')
    
    locations=[]
    d_d_m_x=distance_matrix
    oo=locations_list
    unlocked_unvisted = [item for item in UI1 if item not in visited]
   
    
  
    locs=[] 
    for i in unlocked_unvisted:
   
        from scripts.c_distance import get_distance
        
        d=distance_matrix[current_location][i]
        
        locs.append({"distance":d,"destination":i})
    
    try:
     mm=min(locs, key=lambda obj: obj["distance"])
    
     closest_location=mm["destination"]
    # adding the unlock location to the visited list
     parcelId=locations_mapping1[closest_location]
     if parcelId in locked_entrepot1.keys():
        UI1.append(locked_entrepot1[parcelId])
        locked_entrepot1.pop(parcelId)

     elif parcelId in locked_delivery1.keys():
        
        UI1.append(locked_delivery1[parcelId])
        locked_delivery1.pop(parcelId)
    
     return closest_location
    except Exception as e:
        return None

def tsp2(matrix, start_location,UI1,locations_mapping1,locked_entrepot1,locked_delivery1 ,max_consecutive_pickups):
   
    n = len(matrix)
    visited = [start_location]

      
    
    parcelId=locations_mapping1[start_location]

    
    if parcelId in locked_entrepot1.keys():
        UI1.append(locked_entrepot1[parcelId])
     #  
        locked_entrepot1.pop(parcelId)
    

    elif parcelId in locked_delivery1.keys():
       
       
        UI1.append(locked_delivery1[parcelId])
        locked_delivery1.pop(parcelId)
    

    while len(visited) < n:
        current_location = visited[-1]        

        closest_location = find_closest_unvisited_location(matrix, current_location,
                                                            visited,UI1,locations_mapping1,
                 locked_entrepot1,locked_delivery1, max_consecutive_pickups)
        try:
         if closest_location is not None:
            visited.append(closest_location)
            current_location = closest_location
         else:
            break
        except Exception as e :
           pass
   
    return visited

def get_vehicles_data(key,vehicles):
    payload=0
    speed=0
    for vehicle, details in vehicles.items():
    
        if(key.lower()==vehicle.lower()):
            payload=details['payload']
            speed=details["max_distance"]
            break;
    return payload,speed

def is_vehicle_available(key,vehicles):
   is_available=False
   for v,d in vehicles.items():
      if(key.lower()==v.lower()):
         is_available=True
         break;
  
   return is_available
   
def can_vehicle_carry_item(vehicle, distance, volume,vehicles):
    payload,speed = get_vehicles_data(vehicle,vehicles)
    if volume > payload:
        return False
    if distance > speed:
        return False
    return True


def find_suitable_vehicle_for_runsheet2(distance_matrix,runsheet_,vehicles,init_payload):

    runsheet=[o["locations"] for o in runsheet_]
    indexes=[o["index"] for o in runsheet_]
    tsp_routes_indexes={}
   
    scooter_route=""
    van_route=""
    truck_route=""
    truck_number=0
    van_number=0
    
    payload=init_payload
    tsp_routes=[]
    if(len(runsheet)==1):
        print('')
    arr=[]
    for i in range(0,len(indexes)-1): 
        s=i    
        e=i+1
        
        if(s not in arr and e not in arr):
         arr.append(s)
         arr.append(e)
        
         c=distance_matrix[indexes[s]][indexes[e]]
       
         
         if runsheet[s][0]=="DELIVERY":
           
            payload=payload-runsheet[s][3]
         else:
            payload=payload+runsheet[s][3]
        
        
         if(can_vehicle_carry_item("scooter",float(c),payload,vehicles)):
            waypoints=[runsheet[s][1],runsheet[e][1]]
           
            tsp_routes.append({"vehicle":"scooter","waypoints":waypoints,"indexes":[indexes[s],indexes[e]]})
            scooter_route=scooter_route+runsheet[s][1]+"->"+runsheet[e][1]+"->"
            
         elif(can_vehicle_carry_item("van",float(c),payload,vehicles)):
            waypoints=[runsheet[s][1],runsheet[e][1]]
           
            tsp_routes.append({"vehicle":"van","waypoints":waypoints,"indexes":[indexes[s],indexes[e]]})           
           
         
         elif(can_vehicle_carry_item("truck",float(c),payload,vehicles)):
            if(float(c)>250):
                truck_number=truck_number+11
                
                waypoints=[runsheet[s][1],runsheet[e][1]]
                tsp_routes.append({"vehicle":f"truck{truck_number}","waypoints":waypoints,"indexes":[indexes[s],indexes[e]]})
                truck_route=truck_route+runsheet[s][1]+"->"+runsheet[e][1]+"->"    
            else:   
             waypoints=[runsheet[s][1],runsheet[e][1]]
             tsp_routes.append({"vehicle":"truck","waypoints":waypoints,"indexes":[indexes[s],indexes[e]]})
             truck_route=truck_route+runsheet[s][1]+"->"+runsheet[e][1]+"->"
         elif(can_vehicle_carry_item(mini_van,float(c),payload,vehicles)):
            
            waypoints=[runsheet[s][1],runsheet[e][1]]
            tsp_routes.append({"vehicle":mini_van,"waypoints":waypoints,"indexes":[indexes[s],indexes[e]]})
        
      
    vehicle_routes={"scooter-route":scooter_route,"van-route":van_route,"truck-route":truck_route}
    
    return tsp_routes,vehicle_routes,payload

         
        

        
def find_suitable_vehicle_for_runsheet(distance_matrix,runsheet_,vehicles,init_payload):
  
   
    runsheet=[o["locations"] for o in runsheet_]
    indexes=[o["index"] for o in runsheet_]
    tsp_routes_indexes={}
    
    scooter_route=""
    van_route=""
    truck_route=""
    truck_number=0

    
    payload=init_payload
    tsp_routes=[]
    
    if(len(runsheet)==1):
        print("here is the problem")
    for i in range(0,len(runsheet)-1): 
       # print(runsheet[i][0],runsheet[i][1],runsheet[i][3])
   
        c=distance_matrix[indexes[i]][indexes[i+1]]
      
        
        if runsheet[i][0]=="DELIVERY":
            payload=payload-runsheet[i][3]
        elif runsheet[i][0]=="DELIVERY&PICKUP (entrepot)":
            
            payload=payload-runsheet[i][3]+runsheet[i][3]
    
        else:
    
            payload=payload+runsheet[i][3]
    
        if(can_vehicle_carry_item("scooter",float(c),payload,vehicles)):
            waypoints=[runsheet[i][1],runsheet[i+1][1]]
    
            tsp_routes.append({"vehicle":"scooter","waypoints":waypoints,"indexes":[indexes[i],indexes[i+1]]})
            scooter_route=scooter_route+runsheet[i][1]+"->"+runsheet[i+1][1]+"->"
    
        elif(can_vehicle_carry_item("van",float(c),payload,vehicles)):
            waypoints=[runsheet[i][1],runsheet[i+1][1]]
    
            tsp_routes.append({"vehicle":"van","waypoints":waypoints,"indexes":[indexes[i],indexes[i+1]]})
            van_route=van_route+runsheet[i][1]+"->"+runsheet[i+1][1]+"->"
        elif(can_vehicle_carry_item(mini_van,float(c),payload,vehicles)):
    
            waypoints=[runsheet[i][1],runsheet[i+1][1]]
            tsp_routes.append({"vehicle":mini_van,"waypoints":waypoints,"indexes":[indexes[i],indexes[i+1]]})
        elif(can_vehicle_carry_item("truck",float(c),payload,vehicles)):
            if(float(c)>250):
                truck_number=truck_number+1
    
                waypoints=[runsheet[i][1],runsheet[i+1][1]]
                tsp_routes.append({"vehicle":f"truck{truck_number}","waypoints":waypoints,"indexes":[indexes[i],indexes[i+1]]})
                truck_route=truck_route+runsheet[i][1]+"->"+runsheet[i+1][1]+"->"    
            else:   
             waypoints=[runsheet[i][1],runsheet[i+1][1]]
             tsp_routes.append({"vehicle":"truck","waypoints":waypoints,"indexes":[indexes[i],indexes[i+1]]})
             truck_route=truck_route+runsheet[i][1]+"->"+runsheet[i+1][1]+"->"
        i=i+2
    
    vehicle_routes={"scooter-route":scooter_route,"van-route":van_route,"truck-route":truck_route}
    
    return tsp_routes,vehicle_routes,payload

def check_if_delivery_is_done(missions):
    cond=False
    dstns=[]
    for data in missions:           
           
            cond=False
            for task in data['tasks']:
              
               try:
                if(len(data["tasks"])>=2):   
               
                 if(task["status"]=="NOT_STARTED" and task["action"]=="DELIVERY"):
                        cond=True
                       
                        pickup_task = [t for t in data["tasks"] if t["action"] == "PICKUP"]
                       
                        
                        if("DONE" in pickup_task[0]["status"] or "ASSIGNED" in pickup_task[0]["status"]):
                         
                           Delivery_task = [t for t in data["tasks"] if t["action"] == "DELIVERY"]
                         
                           data["tasks"].remove(Delivery_task[0])
                           data["tasks"].remove(pickup_task[0])
                        elif(pickup_task[0]["status"]=="NOT_STARTED"):
                           data["tasks"].remove(pickup_task[0])


                
                 elif(task["status"]=="DONE" and task["action"]=="PICKUP" or task["status"]=="ASSIGNED" and task["action"]=="PICKUP"):
                     
                     pickup_task = [t for t in data["tasks"] if t["action"] == "PICKUP"]
                  #   print(pickup_task[0]["_id"],"has to be removed")
                     data["tasks"].remove(pickup_task[0])

                     delivery_task=[z for z in data["tasks"] if z["action"] == "DELIVERY" and z["status"]=="NOT_STARTED" or z["action"] == "DELIVERY" and z["status"]=="DONE" or z["action"] == "DELIVERY" and z["status"]=="ASSIGNED"]
                   #  print(delivery_task[0]["_id"],"has to be removed")
                     data["tasks"].remove(delivery_task[0])
               

                 elif(task["status"]=="DONE" and task["action"]=="DELIVERY" ):
                        
                        delivery_task=[z for z in data["tasks"] if z["action"] == "DELIVERY"]
                     #   print(delivery_task[0]["_id"],"has to be removed")
                        data["tasks"].remove(delivery_task[0])

                        pickup_task1 = [t for t in data["tasks"] if t["action"] == "PICKUP" and t["status"]=="DONE" or t["action"] == "PICKUP" and t["status"]=="ASSIGNED"]
                      #  print(pickup_task1[0]["_id"],"has to be removed")
                        data["tasks"].remove(pickup_task1[0])

                 elif(task["status"]=="ASSIGNED" and task["action"]=="DELIVERY"):
                        
                        delivery_task=[z for z in data["tasks"] if z["action"] == "DELIVERY"]
                     #   print(delivery_task[0]["_id"],"has to be removed")
                        data["tasks"].remove(delivery_task[0])

                        pickup_task = [t for t in data["tasks"] if t["action"] == "PICKUP"]
                     #   print(pickup_task[0]["_id"],"has to be removed")
                        data["tasks"].remove(pickup_task[0])
                        

               except Exception as e:
                 pass
                
                
                     
                
                 #for stop in task["stops"]:             
                   #print(stop["address"]["address"],stop["action"])

               
    return missions


def reorder_scooter_indexes(scooter_indexes,locations_list,distance_matrix):
   
    routes=[]    
    for x in scooter_indexes  :        
    #    print(locations_list[x])  
        routes.append(locations_list[x])
    sorted_orders = sorted(routes, key=lambda x: (x[0] != "PICKUP"))
    
    indexes=scooter_indexes
    
    pickups= list(filter(lambda x: x[0] == "PICKUP", routes))
    deliveries= list(filter(lambda x: x[0] == "DELIVERY", routes))
    st1=pickups[0]
    st=locations_list.index(st1)
    l=pickups[len(pickups)-1]
    pickup_indexes=[]
    delivery_indexes=[]
    
    for x in sorted_orders:
    
       if(x[0]=="PICKUP"):
          pickup_indexes.append(locations_list.index(x))
       else:
          delivery_indexes.append(locations_list.index(x))   
    pickups_to_sort=[]
    
    for p_i in pickup_indexes:
    
           pickups_to_sort.append({"distance":distance_matrix[st][p_i],"index":p_i})
    sorted_pickups=list(sorted(pickups_to_sort, key=lambda x: x["distance"],reverse=True))  
    deliveries=[]   
    c=sorted_pickups[len(sorted_pickups)-1]["index"]
    for d_i in delivery_indexes:
    
           deliveries.append({"distance":distance_matrix[c][d_i],"index":d_i})
    sorted_deliveries = list(sorted(deliveries, key=lambda x: x["distance"]))
    print("-----------------")
    new_scooter_indexes=[]
    sf=sorted_pickups[0]
    #print("sf==",sf)
    pick_sort=[]
    other_pick_sort=[]
    for x in sorted_pickups:
       #print(locations_list[x["index"]])
       if(warehouse_address not in locations_list[x["index"]][1].lower()):
    
          pick_sort.append({"distance":distance_matrix[sf["index"]][x["index"]],"index":x['index']})
       else:
          other_pick_sort.append(x["index"])
      # new_scooter_indexes.append(x['index'])
    final_sorted_pickups=list(sorted(pick_sort, key=lambda x: x["distance"]))
    for z in final_sorted_pickups:
    
       new_scooter_indexes.append(z["index"])
    new_scooter_indexes=new_scooter_indexes+other_pick_sort
      
    for d in sorted_deliveries:
    
       new_scooter_indexes.append(d['index'])
    return new_scooter_indexes

    
       
def van_route(el,locations_list):
   for i in locations_list:
      #search pickup source 
      index_t=0
      if(el[2]==i[2]):
      #    print(i,locations_list.index(i))
          index_t=locations_list.index(i)
          break;
   return  index_t

def truck_route(el,locations_list):
   for i in locations_list:
      index_t=0
      if(el[2]==i[2]):
         
          index_t=locations_list.index(i)
          break;
   return  index_t


def truck1_route(el,locations_list):
   for i in locations_list:
      index_t=0
      if(el[2]==i[2]):
        
          index_t=locations_list.index(i)
          break;
   return  index_t

def optimize_scooter_route(scooter_i,locations_list,mtx):   
   locked_delivery={}
   pickups=[]
   visited=[scooter_i[0]]
   location_mapping={}
   for g in scooter_i:
    location_mapping.update({g:locations_list[g][2]})
    
    if(locations_list[g][0]=="DELIVERY"):  
      locked_delivery.update({locations_list[g][2]:g})
    else:
      pickups.append(g)
   
   s=0
   n=len(mtx)
   while len(visited) < len(scooter_i):
    current_location = visited[-1]
    s=s+1
    if(s==len(scooter_i)-1):
       break
    nearest_location=find_closest_unvisited_location(mtx,current_location,visited,pickups,location_mapping,{},locked_delivery,locations_list)  
    if nearest_location is not None:
            visited.append(nearest_location)
            current_location = nearest_location
  #          print(visited)
    else:
            break
   for b in scooter_i:
       if(b not in visited):
          visited.append(b)
   #print(visited)
   return visited  
        
def optimize_van_route(n_van_i,locations_list,mtx):
   locked_delivery={}
   pickups=[]
   visited=[n_van_i[0]]
   location_mapping={}
   UI1= []
   for g in n_van_i:
    location_mapping.update({g:locations_list[g][2]})
   
    if(locations_list[g][0]=="DELIVERY"):  
      locked_delivery.update({locations_list[g][2]:g})
    else:
      pickups.append(g)
      UI1.append(g)
   
   s=0
   n=len(mtx)
   start_location=n_van_i[0]
   v=tsp2(mtx,start_location ,UI1,location_mapping,{},locked_delivery ,locations_list)
   
 
   return v

def optimize_van_route2(n_van_i,locations_list,dm):
   van_routes=[]
   van_routes_indexes=[]
   nb_deliveries=0
   for i in n_van_i:
     
      if(warehouse_address in locations_list[i][1].lower()):
         if(locations_list[i][0]=="DELIVERY"):
            nb_deliveries=nb_deliveries+1
         van_routes.append({"el":locations_list[i],"index":i})
         van_routes_indexes.append(i)
   if(nb_deliveries==len(van_routes)):
      return optimize_van_route(n_van_i,locations_list,dm)
   else:
    sorted_van_routes = list(sorted(van_routes, key=lambda x: (x["el"][0] != "DELIVERY")))
    srt_van_ind= [e["index"] for e in sorted_van_routes]
    other_van_routes=[]
    pickup_ops=[]
    delivery_ops=[]
    for e in n_van_i:
     
      if(e not in van_routes_indexes):
     
         if(locations_list[e][0]=="PICKUP"):
             pickup_ops.append({"index":e,"distance":dm[e][van_routes_indexes[0]]})
         else:
             delivery_ops.append({"index":e,"distance":dm[e][van_routes_indexes[0]]})  

         other_van_routes.append({"index":e,"distance":dm[e][van_routes_indexes[0]]})

    other_van_routes_opt=list(sorted(pickup_ops, key=lambda x: x["distance"],reverse=True))
    other_van_routes_opt2=list(sorted(delivery_ops, key=lambda x: x["distance"]))

   
    
    srt_van_ind1= [e["index"] for e in other_van_routes_opt]
    srt_van_ind2= [e["index"] for e in other_van_routes_opt2]
    listvan_indexes=srt_van_ind1+srt_van_ind+srt_van_ind2
    return listvan_indexes
   
def optimize_pickup_ops(pickup_indexes,dm,locations_list):
   
    
    
    r=optimize_scooter_route(pickup_indexes,locations_list,dm)
    return r

def organize_deliveries(deliveries_rt,dm):
  l=[]
  for x in deliveries_rt:
    
    l.append({"distance":dm[x][deliveries_rt[0]],"dest":x})
  sorted_objects = sorted(l, key=lambda x: x["distance"])
    
    # Extract the values of the extract_key from the sorted list
  extracted_values = [obj["dest"] for obj in sorted_objects]
    
  return extracted_values
ui=[]



def find_shortcut_3(dm,li,locations_list):
 warehouse_address="152 rue dd 49"
 ui_p=[]
 ui=[]
 ui_d=[]
 for l in li :
  ui.append(l[1])
  if(l[0]=="PICKUP"):
    ui_p.append(l)
  else:
    ui_d.append(locations_list.index(l))
 start1=[e for e in ui_p if( warehouse_address in e[1].lower() and e[0].lower()=="pickup")]
 for f in li:
  if(f[2]==start1[0][2]):
    ui_p.append(f)
 
 dakar_waypoints=[]
 other=[]
 for o in ui_p:
  if("dakar" in o[1].lower()):
 
    dakar_waypoints.append(o)
 
  else:
    addr=o
    other.append(locations_list.index(addr))
 mx=10000
 obj=()
 indexes=[]
 for x0 in dakar_waypoints:
  indexes.append(locations_list.index(x0))
 for x in dakar_waypoints:
  
  if(warehouse_address not in x[1].lower() and "rte des maristes" not in x[1].lower()):
 
   if(dm[locations_list.index(x)][locations_list.index(addr)]<mx):
    mx=dm[locations_list.index(x)][locations_list.index(addr)]
    obj=x
 
#***choose starting point***
 mx=0
 starting_points=[]
 starting_point=()
 for z in dakar_waypoints:
  from scripts.find_optimize_route import optimize_scooter_route
  if(z!=obj):
   indexes.remove(locations_list.index(z))
   indexes=[locations_list.index(z)]+indexes
   route=optimize_scooter_route(indexes,locations_list,dm)
   from scripts.distance_calculations import calculate_total_distance
   x=calculate_total_distance(dm,route)
 
   starting_points.append({"location":z,"distance":x,"indexes":indexes})
 

 mn=10000
 for l in starting_points:
  
 
  if(l['distance']<mn):
    mn=l["distance"]
    obj_min=l['indexes']


 r=optimize_scooter_route(obj_min+other,locations_list,dm)
 r.remove(locations_list.index(start1[0]))
 
 deliveries_rt=[r[len(r)-1]]+ui_d
 deliveries_rt.remove(locations_list.index(o))
 
 r2=organize_deliveries(deliveries_rt,dm)
 
 new_route=[]
 for e in [locations_list.index(start1[0])]+r+r2[1:]:
 # if(li[e]!=start1[0]):
 
   new_route.append(e)
 return new_route

 

def optimize_scooter_route2(scooter_indexes,locations_list,dm):
   scooter_routes=[]
   scooter_routes_indexes=[]
   nb_deliveries=0
   for i in scooter_indexes:
 
      
      if(warehouse_address in locations_list[i][1].lower()):
         if(locations_list[i][0]=="DELIVERY"):
            nb_deliveries=nb_deliveries+1   
         scooter_routes.append({"el":locations_list[i],"index":i})
         scooter_routes_indexes.append(i)
   sorted_scooter_routes = list(sorted(scooter_routes, key=lambda x: (x["el"][0] != "DELIVERY")))
   if(nb_deliveries==len(scooter_routes)):
      return optimize_scooter_route(scooter_indexes,locations_list,dm)
   srt_scooter_ind= [e["index"] for e in sorted_scooter_routes]
   other_scooter_routes=[]
   pickup_ops=[]
   delivery_ops=[]
   for e in scooter_indexes:
 
      if(e not in scooter_routes_indexes):
          if(locations_list[e][0]=="PICKUP"):
             pickup_ops.append({"index":e,"distance":dm[e][scooter_routes_indexes[0]]})
          else:
             delivery_ops.append({"index":e,"distance":dm[e][scooter_routes_indexes[0]]})  

          other_scooter_routes.append({"index":e,"distance":dm[e][scooter_routes_indexes[0]]})
   if(len(pickup_ops)>=3):
                        other_scooter_routes_opt=list(sorted(pickup_ops, key=lambda x: x["distance"],reverse=True))
                        other_scooter_routes_optI=list(sorted(pickup_ops, key=lambda x: x["distance"]))
                        other_scooter_routes_opt2=list(sorted(delivery_ops, key=lambda x: x["distance"]))
                      
                        srt_scooter_ind1= [e["index"] for e in other_scooter_routes_opt]
                        srt_scooter_indI= [e["index"] for e in other_scooter_routes_optI]
                        srt_scooter_ind2= [e["index"] for e in other_scooter_routes_opt2]
                        pickup_line=srt_scooter_ind1
                        pickups=optimize_pickup_ops(pickup_line,dm,locations_list)
                        listscooter_indexes=pickups+srt_scooter_ind+srt_scooter_ind2
                        
         
   else : 
  
    other_scooter_routes_opt=list(sorted(pickup_ops, key=lambda x: x["distance"]))
    other_scooter_routes_opt2=list(sorted(delivery_ops, key=lambda x: x["distance"]))
    srt_scooter_ind1= [e["index"] for e in other_scooter_routes_opt]
    srt_scooter_ind2= [e["index"] for e in other_scooter_routes_opt2]
    listscooter_indexes=srt_scooter_ind1+srt_scooter_ind+srt_scooter_ind2
    
   return listscooter_indexes
   

def double_check_van(new_van_indexes,vehicles_list,locations_list,dm,new_scooter_indexes):
    
         
         payload=0
         to_test=[]
         c=0
         to_test_index=[]
         for x in new_van_indexes:
    
            c=c+1
            if(warehouse_address in locations_list[x][1].lower() and locations_list[x][0]=="PICKUP"):
                to_test.append(locations_list[x])
                to_test_index.append(x)
                
                   
    
         
         for i in to_test:
             indx=locations_list.index(i)
    
             t=0
             for x in new_van_indexes:
                if(indx!=x and locations_list[indx][2]==locations_list[x][2]):
    
                     t=x
                     break;
          
    
             if(locations_list[indx][0]=="PICKUP"):
                 payload=payload+locations_list[indx][3]
             
             if("dakar" in locations_list[t][1].lower() and locations_list[t][0]=="DELIVERY" and locations_list[t][2]==locations_list[indx][2]):
              d=dm[indx][t]
              cond=can_vehicle_carry_item("scooter",payload,d,vehicles_list)
        
              if(cond==True):
                 new_scooter_indexes.append(indx)
                 new_scooter_indexes.append(t)
                 new_van_indexes.remove(indx)
                 new_van_indexes.remove(t)
             
    
def get_deliveries_shortcut_routes(van_indexes,locations_list,mtx,deliveries):
   
   distances=[]
   for e in van_indexes:
      dst=mtx[e][locations_list.index(deliveries[0])]
    
      distances.append({"location":e,"distance":dst})
   sorted_distances = sorted(distances, key=lambda x: x["distance"],reverse=True)
   sorted_locations = [item["location"] for item in sorted_distances]
   
# Display the sorted list
   return sorted_locations

def generate_vans_routes(van_routes,locations_list,mtx,deliveries):
 
 ls=van_routes
 n=get_calculations_params(deliveries)
 
 runsheets_init=choose_start_points(deliveries,ls,locations_list,mtx,n)
 indexed_runsheets=[]
 final_runsheets=[]
 visited=[]
 for rnst in runsheets_init:
   st=locations_list.index(rnst[0])
   ed=locations_list.index(rnst[1])
   if(st in visited and ed in visited):
      print("*------------end----------------------*")
   else:
    r=find_shortcut(rnst,n,deliveries,locations_list,mtx)
    p=find_pickup_ops(r,ls)
    
    runsheet=[]
    indexed_runsheet=[]
   
      
    for element in p+r : 
     
     runsheet.append(element)
     if(locations_list.index(element) not in indexed_runsheet):
      indexed_runsheet.append(locations_list.index(element))
      visited.append(locations_list.index(element))
    
    final_runsheets.append(runsheet)
   if(indexed_runsheet not in indexed_runsheets):
     indexed_runsheets.append(indexed_runsheet)
   if(len(runsheet)==len(ls)):
      break;
   
 unused_runsheet=[]

 all_addresses= []
 unreached_locs=[]
 for f in final_runsheets:
   for ix in f:
     all_addresses.append(ix)
 for x in ls:
    if(x not in all_addresses):
     if(x[0]=="PICKUP" and warehouse_address.lower() not in x[1].lower()):
   
      unreached_locs.append(locations_list.index(x))
      for z in locations_list:
         if(z[2]==x[2] and locations_list.index(z) not in unreached_locs):
            unreached_locs.append(locations_list.index(z))
            
     else:
      unused_runsheet.append(x)
       

 
 try:
  indexes=[]
  o=()
 
  for i in unused_runsheet:
    
    indexes.append(locations_list.index(i))
    if(i[0]=="PICKUP" and warehouse_address.lower() in i[1].lower()):
        o=locations_list.index(i)
  indexes.remove(o)
  indexes=[o]+indexes

 
 
  r=optimize_van_route(indexes,locations_list,mtx)
 
  dst=0
  products=[]
 
  n_r=[]
  for j in r:
  # print(locations_list[j])
   
   n_r.append(j)
   dst=dst+calculate_total_distance(mtx, r[0:r.index(j)])
   if(dst>=200):
     break;
 
  for j1 in n_r:
    products.append(locations_list[j1][2])
  n_r1=[]    
  for j0 in n_r:
 
      if(products.count(locations_list[j0][2])==1):
        print("")
      else:
      #print(products.count(locations_list[j0][2]))
 
       n_r1.append(j0)

  r=n_r1
  indexed_runsheets.append(r)
  
 

  return indexed_runsheets,unreached_locs
 except Exception as e : 
 
    r=[]
    for i in unused_runsheet:
       if(i not in r):
        r.append(locations_list.index(i))
 return indexed_runsheets,r


def generate_truck_runsheets(truck_routes,locations_list,mtx,deliveries):
 ls=truck_routes
 visited=[]
 runsheets_init=choose_start_points2(deliveries,ls,locations_list,mtx)
 
 
 indexed_runsheets=[]
 final_runsheets=[]
 
 for rnst in runsheets_init:
   st=locations_list.index(rnst[0])
   ed=locations_list.index(rnst[1])
   r=find_shortcut_truck(rnst,3,deliveries,locations_list,mtx)
   p=find_pickup_ops(r,ls)
 
   runsheet=[]
   indexed_runsheet=[]
   if(st in visited and ed in visited):
      print("*------------end----------------------*")
   else:   
    for element in p+r : 
     #print(element)
     runsheet.append(element)
    
     indexed_runsheet.append(locations_list.index(element))
     visited.append(locations_list.index(element))
    #print(indexed_runsheet)
    final_runsheets.append(runsheet)
   
   indexed_runsheets.append(indexed_runsheet)
   if(len(runsheet)==len(ls)):
      break;
 
 unused_runsheet=[]

 all_addresses= []
 for f in final_runsheets:

   for ix in f:
     all_addresses.append(ix)
 for x in ls:
    if(x not in all_addresses):
 #     print(x)
      unused_runsheet.append(x)
       


 try:
  indexes=[]
  o=()
 
  for i in unused_runsheet:

    indexes.append(locations_list.index(i))
    if(i[0]=="PICKUP" and warehouse_address.lower() in i[1].lower()):
        o=locations_list.index(i)
  indexes.remove(o)
  indexes=[o]+indexes

 

  r=optimize_van_route(indexes,locations_list,mtx)

 
 except Exception as e : 
 
    r=[]
    for i in unused_runsheet:
       r.append(locations_list.index(i))
 return indexed_runsheets,r

def check_address(address):
   warehouse_region="dakar"
   if(warehouse_region in address.lower() and warehouse_address.lower() not in address.lower()):
      return True
   else:
      return False

def return_runsheets2(data_missions,distance_matrix,locations_list,locked_delivery,locked_entrepot,vehicles_list):
   print("------------------------")
   products=[]
   warehouse_region="dakar"
   d_t_m=data_missions
 
   for element in locations_list:
     if(element[2] not in products):
      products.append(element[2])
   waypoints=[]
   for x in products:
      for i in locations_list:
         if(i[2]==x):
 
            waypoints.append(i)
 
   possible_scooter_routes=[]
   possible_runsheet_scooter=[]
   other_runsheet=[]
   other_routes=[]
   for w in range(0,len(waypoints)-1):
      if(warehouse_region in waypoints[w][1].lower() and warehouse_region in waypoints[w+1][1].lower() and waypoints[w][2]==waypoints[w+1][2]):
         possible_scooter_routes.append(waypoints[w])
         possible_scooter_routes.append(waypoints[w+1])
         possible_runsheet_scooter.append({"index":locations_list.index(waypoints[w]),"locations":waypoints[w]})
         possible_runsheet_scooter.append({"index":locations_list.index(waypoints[w+1]),"locations":waypoints[w+1]})
   other_routes=[]
   for e in waypoints:
      if(e not in possible_scooter_routes):
         other_routes.append(e)
         other_runsheet.append({"index":locations_list.index(e),"locations":e})
    
 
   
   try:
    t_s,v_r,p=find_suitable_vehicle_for_runsheet2(distance_matrix,possible_runsheet_scooter,vehicles_list,0)
    t_s1,v_r1,p1=find_suitable_vehicle_for_runsheet2(distance_matrix,other_runsheet,vehicles_list,0)
   except Exception as E : 
      print("")
   scooter_indexes=[]
   van_indexes=[]
   truck_indexes=[]
   truck1_indexes=[]
   tsp_routes_indexes={}
   for t1 in t_s:
                  if(t1["vehicle"]=="scooter"):
                  #  print(t1["vehicle"],t1["indexes"])
                    for index in t1["indexes"]:
                      
                        scooter_indexes.append(index) 
                
                  if("van" in t1["vehicle"]):
                   # print(t1["vehicle"],t1["indexes"])
                    for index in t1["indexes"]:
                     
                        van_indexes.append(index)

                  if(t1["vehicle"]=="truck"):
                    #print(t1["vehicle"],t1["indexes"])
                    for index in t1["indexes"]:
                      
                        truck_indexes.append(index)
   van_routs=[]              
   for t2 in t_s1:
                  if(t2["vehicle"]=="scooter"):
                    #print(t2["vehicle"],t2["indexes"])
                    for index in t2["indexes"]:
                        scooter_indexes.append(index) 
                      
                  if("van" in t2["vehicle"]):
                    #print(t2["vehicle"],t2["indexes"])
                    for index in t2["indexes"]:
                        van_indexes.append(index)
                  
                  
                  if("truck" in t2["vehicle"].lower()):
                    #print(t2["vehicle"],t2["indexes"])
                    for index in t2["indexes"]:
                        truck_indexes.append(index)
                  
                  
                  if(t2["vehicle"]=="truck1"):
                    #print(t2["vehicle"],t2["indexes"])
                    for index in t2["indexes"]:
                        truck1_indexes.append(index)
   
   
   for vans in van_routs:
     van_indexes=vans["indexes"]
     van_key=vans["van_nb"]
   new_scooter_indexes=[]
   if(len(scooter_indexes)>0):  
     try:                  
      new_scooter_indexes=optimize_scooter_route2(scooter_indexes,locations_list,distance_matrix) 
      tsp_routes_indexes.update({'scooter':new_scooter_indexes})
     except Exception as e : 
        print('')
   if(len(van_indexes)>0):
     #print("van_indexes=======",van_indexes)
     van_routesII=[]
     deliveries=[]
     for x in van_indexes:
      #   print(x)
         van_routesII.append(locations_list[x])
         if(locations_list[x][0]=="DELIVERY"):
             deliveries.append(locations_list[x])
     #print("van_routesII===",van_routesII)
     #print("deliveries===",deliveries)
     if(len(van_routesII)<=2):
        tsp_routes_indexes.update({"van":van_indexes})
     elif(get_calculations_params(deliveries)==0):
        r=get_deliveries_shortcut_routes(van_indexes,locations_list,distance_matrix,deliveries)
      
        tsp_routes_indexes.update({"van":r})
     else:      
      
      r,I = generate_vans_routes(van_routesII,locations_list,distance_matrix,deliveries)
     
      van_number=0
      for e in r : 
       van_number=van_number+1
       tsp_routes_indexes.update({f"van{van_number}":e})
      if(len(I)>0):
       tsp_routes_indexes.update({f"van{van_number+1}":I})
      try:
       indexes_to_reorder=new_scooter_indexes+I
       route_vn_sc=[]
       for i1 in indexes_to_reorder:
      #   print("i1========================",locations_list[i1])
         route_vn_sc.append(locations_list[i1])
       #print(indexes_to_reorder)
       r=find_shortcut_3(distance_matrix,route_vn_sc,locations_list)
       
       
       w=0
       for to_test in r:
         if(locations_list[to_test][0]=="PICKUP"):
           w=w+locations_list[to_test][3]
         

       if(can_vehicle_carry_item("van",0,w,vehicles_list)):
         
         tsp_routes_indexes.update({f"van{van_number+1}":r})
         

         key_to_delete = 'scooter'

         if key_to_delete in tsp_routes_indexes:
            del tsp_routes_indexes[key_to_delete]
      
   
     
      except Exception as E:
        print("")
      

   if(len(truck_indexes)>2):
     #print("truck---------------------")
     truck_routesII=[]
     truck_deliveries=[]
     for x in truck_indexes:
      
           truck_routesII.append(locations_list[x])
           if(locations_list[x][0]=="DELIVERY"):
             truck_deliveries.append(locations_list[x])
     #print("van_routesII===",truck_routesII)
     #print("deliveries===",truck_deliveries)
     if(get_calculations_params(truck_deliveries)==0):
        r=get_deliveries_shortcut_routes(truck_indexes,locations_list,distance_matrix,truck_deliveries)
      #  print(r)
        tsp_routes_indexes.update({"truck":r})
     else:      
      r,I = generate_truck_runsheets(truck_routesII,locations_list,distance_matrix,truck_deliveries)
    
     # print(r,I)
      truck_number=0
      for e in r : 
       
       if(len(e)>0):
        truck_number=truck_number+1
        tsp_routes_indexes.update({f"truck{truck_number}":e})

      if(len(I)>0):
       tsp_routes_indexes.update({f"truck{truck_number+1}":I})
     
      indexes_to_reorder=new_scooter_indexes+I
      #print(indexes_to_reorder)
      b=[]
      cc=[]
      if(len(van_indexes)==0):
       start=0
       for nw_indx0 in indexes_to_reorder:
        if(locations_list[nw_indx0][0]=="PICKUP" and warehouse_address in locations_list[nw_indx0][1].lower()):
           start=nw_indx0
           break;
       indexes_to_reorder.remove(start)
       indexes_to_reorder=[start]+indexes_to_reorder
       for nw_indx in indexes_to_reorder:
        if(locations_list[nw_indx][0]=="PICKUP" or check_address(locations_list[nw_indx][1])==True):
           b.append(nw_indx)
        if(locations_list[nw_indx][0]=="DELIVERY" and check_address(locations_list[nw_indx][1])==False):
           cc.append(nw_indx)
       rt=optimize_van_route(b,locations_list,distance_matrix)
       #for i in rt:
        #print(locations_list[i])
       distances=[]
       for x in cc:
         distances.append({"distance":distance_matrix[x][i],"destination":x})
       delivery_start_location = min(distances, key=lambda x: x['distance'])["destination"]
       cc.remove(delivery_start_location)
       cc=[delivery_start_location]+cc
       rt2=optimize_pickup_ops(cc,distance_matrix,locations_list)
      # for hhh in rt2:
       #  print(locations_list[hhh])
       w=0
       for to_test in rt+rt2:
         w=w+locations_list[to_test][3]
       if(can_vehicle_carry_item("truck",0,w,vehicles_list)):
         tsp_routes_indexes.update({f"truck{truck_number+1}":rt+rt2})
         key_to_delete = 'scooter'
         key_to_delete2='truck'
         if key_to_delete in tsp_routes_indexes:
            del tsp_routes_indexes[key_to_delete]
         if key_to_delete2 in tsp_routes_indexes:
            del tsp_routes_indexes[key_to_delete2]
   else:
      if(len(truck_indexes)>0):
         tsp_routes_indexes.update({'truck':truck_indexes})
    

     
   try:
        print("..")
      #  element_e=locations_list[scooter_indexes[len(scooter_indexes)-1]]
     #   if(warehouse_address in element_e[1].lower() and element_e[0]=='DELIVERY'):
       #  double_check_van(new_van_indexes,vehicles_list,locations_list,distance_matrix,new_scooter_indexes) 
        #tsp_routes_indexes.update({'scooter':new_scooter_indexes})
   except Exception as e :
        print("")
    # tsp_routes_indexes.update({van_key:new_van_indexes})
    
    
        #print("-------------------------------")
        
        #print("-------------------------------")
  
   if(len(truck1_indexes)>0):
    tsp_routes_indexes.update({'truck1':truck1_indexes})
   
   
   return tsp_routes_indexes           
     
     
   
    
    
      
     
   

   








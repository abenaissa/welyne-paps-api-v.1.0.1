import requests

def get_distance(origin, destination):
    base_url = "https://maps.googleapis.com/maps/api/distancematrix/json"
    api_key="AIzaSyAgaRnl5RlSg1bX79_CH3E3xchf_bgA6Gw"
    params = {
        "origins": origin,
        "destinations": destination,
        "key": api_key
    }
    response = requests.get(base_url, params=params)
    data = response.json()
    #print(data)
    if(origin==destination):
        return 0
    if data["status"] == "OK":
        distance = data["rows"][0]["elements"][0]["distance"]["text"]
       
        return float(distance.replace("km",''))
    else:
        print('error')
        return  "Error fetching distance"
    




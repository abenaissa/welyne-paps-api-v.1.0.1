from scripts.main_functions  import seconds_to_hms ,calculate_distances ,split_route,get_route_distances    ,generate_google_maps_link  ,calculate_total_distance   ,choose_starting_point,         combine_consecutive_occurrences
#rom Tsp_modified import tsp 
import numpy as np

from scripts.find_optimize_route import tsp2
def get_coordinates(locations_list):
 
    locations_fit_to_create_DM=[]
    locations_mappping={}
    pickups_mappping={}
    locked_entropot={}
    locked_delivery={}
    wights_mapping={}
    UI=[]
    
   
    for i,element in enumerate (locations_list):
        print(i,"",element)
       
        locations_fit_to_create_DM.append(element[1]) 
        locations_mappping.update({i:element[2]})
        wights_mapping.update({element[2]: element[3]})

        
        if element[0]=='PICKUP':
            

            UI.append(i)
            pickups_mappping.update({element[2]:i})


        elif element[0]=='DELIVERY&PICKUP (ENTROPOT)':
              
            locked_entropot.update({element[2]:i})
           


        else:
            locked_delivery.update({element[2]:i})

    return locations_fit_to_create_DM,locations_mappping,pickups_mappping,locked_entropot,locked_delivery,wights_mapping,UI

def load_vehicles_data(vehicles_list):
    print(vehicles_list)
    for vehicle, details in vehicles_list.items():
        print(vehicle)
        print("Payload:", details["payload"])
        print("Max Distance:", details["max_distance"])

def get_vehicles_data(key,vehicles_list):
    payload=0
    speed=0
    for vehicle, details in vehicles_list.items():
        print(vehicle)
        print("Payload:", details["payload"])
        print("Max Distance:", details["max_distance"])
        if(key.lower()==vehicle.lower()):
            payload=details['payload']
            speed=details["max_distance"]
            break;
    return payload,speed
    

def optimize_delivery_multiple_missions(data_missions,locations_list,mission_tasks, entropot_data,vehicles_list,GOOGLE_MAPS_API_KEY='AIzaSyAgaRnl5RlSg1bX79_CH3E3xchf_bgA6Gw'):
    
    vehicleType=[]
    Data_list=[]
    google_maps_link_list_all=[]
    results = []
    locations_fit_to_create_DM,locations_mappping,pickups_mappping,locked_entropot,locked_delivery,wights_mapping,UI=get_coordinates(locations_list)
    print("locations_fit_to_create_DM",locations_fit_to_create_DM)
    print("ui=",UI)
    print("locked_delivery=",locked_delivery)
    print("locked_warehouse=",locked_entropot)
    print(locations_mappping)
    UI1=UI
    locked_delivery1=locked_delivery
    locked_entropot1=locked_entropot
    locations_mappping1=locations_mappping
 #-------------------------------------------------------------------------------------------------------------




    import requests
    from flask import jsonify
    code_err="nothing"
    try:
     distance_matrix  = np.array(calculate_distances(locations_fit_to_create_DM, GOOGLE_MAPS_API_KEY))
     distance_matrix1=distance_matrix
     e_copy=locked_entropot.copy()
     print("distance_matrix1==",distance_matrix1)
     
    
     from scripts.find_optimize_route import return_runsheets2
    
     tsp_routes_indexes=return_runsheets2(data_missions,distance_matrix1,locations_list,locked_delivery1,locked_entropot1,vehicles_list)
     result1 = ""
     
    
    

     tsp_routes=tsp_routes_indexes  
     for routes_key in tsp_routes:
        Data=[]
        if routes_key.startswith('scooter'):
            vehicleType.append('SCOOTER')
        else:
            if routes_key.startswith('van'):
               vehicleType.append('van')
            if routes_key.startswith('truck'):
               vehicleType.append('TRUCK')
            if routes_key.startswith('Mini van'):
               vehicleType.append('MINI VAN')
                
            
     
        result = ""
     
        if tsp_routes[routes_key]:
            total_distance= calculate_total_distance(distance_matrix, tsp_routes[routes_key])



            
            route_names = [locations_fit_to_create_DM[loc_idx] for loc_idx in tsp_routes[routes_key]]

            

           
            for loc_idx in tsp_routes[routes_key]:
                
               

                if loc_idx in e_copy.values():
                    
                    Data.append(mission_tasks[loc_idx])
                   
                    Data.append(entropot_data[loc_idx])
                else:
                    Data.append(mission_tasks[loc_idx])
          



            
            final_route_name=combine_consecutive_occurrences(route_names)
        
                       
            if len(final_route_name)>25:
                    google_maps_link_list = {}
                    google_maps_link_list['Etape 1'] = generate_google_maps_link(final_route_name[:len(final_route_name)])
                    google_maps_link_list['Etape 2'] = generate_google_maps_link(final_route_name[len(final_route_name):])

                    
            else:
                google_maps_link_list = []
                google_maps_link = generate_google_maps_link(final_route_name)
                google_maps_link_list.append(google_maps_link)

            result += f"Optimal route for {routes_key}: {' --> '.join(final_route_name)} <br>"
            result += f"Total distance : {total_distance:.2f} kilometers <br> <br>"

        else:
            result = "No solution found."

        results.append(result)
        Data_list.append(Data)
        google_maps_link_list_all.append(google_maps_link_list)
    
      
     final_google_maps_link_list_1=""       
     return vehicleType,results,Data_list,google_maps_link_list_all,final_google_maps_link_list_1

    except  Exception as err:
        code_err="network error"
        print("========",str(err),"=========")
        
        return jsonify({
            'status': 'error',
            'code': 400,
            'message': str(err)
        }),400
    
   
    